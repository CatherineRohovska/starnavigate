//
//  StarListViewController.h
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarListViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate>
@end
