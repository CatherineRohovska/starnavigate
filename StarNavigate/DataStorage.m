//
//  DataStorage.m
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "DataStorage.h"

@implementation DataStorage
+(id) sharedStorage{
    static DataStorage *sharedStorage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStorage = [[self alloc] init];
    });
    return sharedStorage;
}

- (id)init {
    if (self = [super init]) {
        //creating url
        NSURL *sourceURL = [NSURL URLWithString:@"http://www.lizard-tail.com/isana/lab/astro_calc/js/bsc_json_short.txt"];
        //loading data
        NSData* sourceData = [NSData dataWithContentsOfURL:sourceURL];
        //creating data
        NSDictionary * allData = [NSJSONSerialization JSONObjectWithData:sourceData options:0 error:nil];
        //parsing data to array
        self.data = [allData objectForKey:@"object"];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:NO];
        self.data=[self.data sortedArrayUsingDescriptors:@[sort]];
     
        
    }
    return self;
}
-(NSArray*) getData{
    return self.data;
}
@end
