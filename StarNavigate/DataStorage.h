//
//  DataStorage.h
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataStorage : NSObject
{

}
@property(nonatomic) NSArray* data;
+(id) sharedStorage;
- (NSArray*) getData;
@end
