//
//  StarListViewController.m
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "StarListViewController.h"
#import "DataStorage.h"
@interface StarListViewController ()
{
    UITableView* starsTable;
    NSArray * starsData;
}

@end

@implementation StarListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //creating table
    starsTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    starsTable.delegate = self;
    starsTable.dataSource = self;
    [starsTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Star"];
    [self.view addSubview:starsTable];
    starsData = [[DataStorage sharedStorage] getData];
    //creating url
//    NSURL *sourceURL = [NSURL URLWithString:@"http://www.lizard-tail.com/isana/lab/astro_calc/js/bsc_json_short.txt"];
//    //loading data
//    NSData* sourceData = [NSData dataWithContentsOfURL:sourceURL];
//    //creating data
//    NSDictionary * allData = [NSJSONSerialization JSONObjectWithData:sourceData options:0 error:nil];
//    //parsing data to array
//    starsData = [allData objectForKey:@"object"];
//    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:NO];
//    starsData=[starsData sortedArrayUsingDescriptors:@[sort]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return starsData.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Star" forIndexPath:indexPath];
    NSDictionary* item = [starsData objectAtIndex:indexPath.item];
   // starsData sor
    NSString* name = [item objectForKey:@"Name"];
    if ([name isEqual: @""]){
         name = [item objectForKey:@"HR"]; //set Hertzsprung-Russell Diagram position instead off name
        
    }
    [cell.textLabel setText:name];
    
    return cell;
}

@end
