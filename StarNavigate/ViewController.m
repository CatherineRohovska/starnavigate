//
//  ViewController.m
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "ViewController.h"
#import "DataStorage.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   [DataStorage sharedStorage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToStarsList:(id)sender {
    StarListViewController* ctrl = [[StarListViewController alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}
@end
